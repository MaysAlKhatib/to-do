import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:get/get.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;
import 'package:to_do_app/models/task.dart';
import 'package:to_do_app/view/notification_page.dart';

class NotifyHelper {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  initializeNotification() async {
    _configureLocalTimeZone();
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            requestSoundPermission: false,
            requestBadgePermission: false,
            requestAlertPermission: false,
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);

    final AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings("@mipmap/ic_launcher");
    final InitializationSettings initializationSettings =
        InitializationSettings(
      iOS: initializationSettingsIOS,
      android: initializationSettingsAndroid,
    );
    final details =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
    if (details != null && details.didNotificationLaunchApp) {
      selectNotification(details.payload);
    }
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }

  displayNotification({required String title, required String body}) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '123', 'theme changed',
        channelDescription: 'dark or light mode',
        importance: Importance.max,
        priority: Priority.high);

    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0, //unique id for each notification
      title,
      body,
      platformChannelSpecifics,
      payload: title,
    );
  }

  scheduledNotification(
      int hour, int minutes, int id, Task task, String type) async {
    String taskJsonString = task.toJsonString();

    await flutterLocalNotificationsPlugin.zonedSchedule(
        id,
        getNotificationTitle(type, task.title!, task.remind!),
        getNotificationBody(type, task.title!, task.note!),
        _convertTime(hour, minutes),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'your channel id', 'your channel name',
              channelDescription: 'your channel description'), // iOS details
          iOS: IOSNotificationDetails(
            sound: 'default.wav',
            presentAlert: true,
            presentBadge: true,
            presentSound:
                true, // Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
          ),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: getDateTimeComponents(task.repeat!),
        payload: taskJsonString);
  }

  durationScheduledNotification(
      {required int id, required Task task, required Duration duration}) async {
    String taskJsonString = task.toJsonString();

    await flutterLocalNotificationsPlugin.zonedSchedule(
        id,
        task.title!,
        task.note!,
        tz.TZDateTime.now(tz.local).add(duration),
        const NotificationDetails(
          android: AndroidNotificationDetails(
              'your channel id', 'your channel name',
              channelDescription: 'your channel description'), // iOS details
          iOS: IOSNotificationDetails(
            sound: 'default.wav',
            presentAlert: true,
            // Present an alert when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
            presentBadge: true,
            // Present the badge number when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
            presentSound:
                true, // Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
          ),
        ),
        androidAllowWhileIdle:
            //specifies if the notification should be sent even when the device is in low power idle mode
            true, //To show notification even when the app is closed
        uiLocalNotificationDateInterpretation:
            //used in iOS versions below 10 (for lack of support) to interpret the time as absolute time or wall clock time
            UILocalNotificationDateInterpretation.absoluteTime,
        payload: taskJsonString);
  }

  DateTimeComponents? getDateTimeComponents(String repeat) {
    if (repeat == 'Daily') {
      return DateTimeComponents.time;
    } else if (repeat == 'Weekly') {
      return DateTimeComponents.dayOfWeekAndTime;
    } else if (repeat == 'Monthly') {
      return DateTimeComponents.dayOfMonthAndTime;
    }
  }

  tz.TZDateTime _convertTime(int hour, int minutes) {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduleDate =
        tz.TZDateTime(tz.local, now.year, now.month, now.day, hour, minutes);
    if (scheduleDate.isBefore(now)) {
      scheduleDate = scheduleDate.add(const Duration(days: 1));
    }
    return scheduleDate;
  }

  Future<void> _configureLocalTimeZone() async {
    tz.initializeTimeZones();
    final String timeZone = await FlutterNativeTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZone));
  }

  void requestIOSPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  Future selectNotification(String? payload) async {
    if (payload == "Theme changed") {
      print("Nothing navigate to ");
    } else {
      Get.toNamed(NotificationPage.routeName, arguments: payload);
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    if (payload == "Theme changed") {
      print("Nothing navigate to ");
    } else {
      Get.toNamed(NotificationPage.routeName, arguments: payload);
    }
  }

  Future<void> cancelNotifications(int id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  String getNotificationTitle(String type, String title, int remind) {
    switch (type) {
      case 'startTime':
        return title;
      case 'endTime':
        return 'Have you finished your task?';
      case 'reminder':
        return '$remind minutes to start your task';
      default:
        return title;
    }
  }

  String getNotificationBody(String type, String title, String body) {
    switch (type) {
      case 'startTime':
        return body;
      case 'endTime':
        return title;
      case 'reminder':
        return title;
      default:
        return title;
    }
  }
}
