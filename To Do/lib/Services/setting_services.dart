import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';
import 'package:get_storage/get_storage.dart';
import 'package:to_do_app/Services/notifications_services.dart';
import 'package:to_do_app/db/db_helper.dart';

class SettingServices extends GetxService{
  var notifyHelper;
  var box ;

  Future<SettingServices> init() async {
    //start service
    await DBHelper.initDB();
    await GetStorage.init();

    box = GetStorage();
    notifyHelper = NotifyHelper();
    notifyHelper.initializeNotification();
    notifyHelper.requestIOSPermissions();
    //end
    return this;
  }

}
