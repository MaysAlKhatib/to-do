import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:to_do_app/Services/setting_services.dart';

class ThemeServices {
  final _key = 'isDarkMode';
  SettingServices _settingServices = Get.find();

  _saveThemeToBox(bool isDarkMode) => _settingServices.box.write(_key, isDarkMode);
  bool _loadThemeFromBox() => _settingServices.box.read(_key) ?? false;
  ThemeMode get theme => _loadThemeFromBox() ? ThemeMode.dark : ThemeMode.light;

  void switchTheme() {
    Get.changeThemeMode(_loadThemeFromBox() ? ThemeMode.light : ThemeMode.dark);
    _saveThemeToBox(!_loadThemeFromBox());
  }
}
