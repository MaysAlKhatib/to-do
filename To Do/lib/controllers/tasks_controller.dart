import 'package:get/get.dart';
import 'package:to_do_app/Services/setting_services.dart';
import 'package:to_do_app/db/db_helper.dart';
import 'package:to_do_app/models/notification.dart';
import 'package:to_do_app/models/task.dart';

class TasksController extends GetxController {
  var taskList = <Task>[].obs;
  var notificationList = <NotificationModel>[].obs;
  var noTasks = 1.obs;
  var selectedDate = DateTime.now().obs;
  SettingServices settingServices = Get.find();

  @override
  Future<void> onInit() async {
    final details = await settingServices
        .notifyHelper.flutterLocalNotificationsPlugin
        .getNotificationAppLaunchDetails();
    if (details != null && details.didNotificationLaunchApp) {
      settingServices.notifyHelper.selectNotification(details.payload);
    }
    getTasks();
    super.onInit();
  }

  void getTasks() async {
    List<Map<String, dynamic>> tasks =
        await DBHelper.queryTasks(selectedDate.value);
    taskList.assignAll(tasks.map((data) => new Task.fromJson(data)).toList());
  }

  Future<void> deleteTask(Task task) async {
    var result = await DBHelper.queryTaskNotifications(task);

    result.forEach((row) async {
      await settingServices.notifyHelper.cancelNotifications(row['id']);
      await DBHelper.deleteNotificationDB(row['id']);
    });
    DBHelper.delete(task);
    getTasks();
  }

  void markTaskCompleted(int id, int finishNotificationId) async {
    await DBHelper.update(id);
    await deleteNotification(finishNotificationId);
    getTasks();
  }

  Future<void> deleteNotification(int id) async {
    await DBHelper.deleteNotificationDB(id);
    await settingServices.notifyHelper.cancelNotifications(id);
  }

  void changeThemeNotification() {
    settingServices.notifyHelper.displayNotification(
        title: "Theme changed",
        body: Get.isDarkMode
            ? "Light mode is activated"
            : "Night mode is activated");
  }
}
