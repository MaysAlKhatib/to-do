import 'package:get/get.dart';
import 'package:to_do_app/Services/setting_services.dart';
import 'package:to_do_app/db/db_helper.dart';
import 'package:to_do_app/models/notification.dart';
import 'package:to_do_app/models/task.dart';

class NotificationPageController extends GetxController {
  int? id;
  var task = Task().obs;
  final isControllerReady = false.obs;
  final remindMe = false.obs;
  var duration = Duration(minutes: 30).obs;
  SettingServices settingServices = Get.find();

  @override
  Future<void> onInit() async {
    _getArgument();
    await _getTask();
    isControllerReady(true);
    super.onInit();
  }

  _getTask() async {
    task.value = await DBHelper.getTaskById(id!);
  }

  _getArgument() {
    String? data = Get.arguments;
    id = Task.fromJsonString(data!).id;
  }

  remindMeLater() async {
    NotificationModel notification;
    notification = await _addTaskNotificationToDb();
    settingServices.notifyHelper.durationScheduledNotification(
        id: notification.id!.toInt(),
        task: task.value,
        duration: duration.value);
  }

  _addTaskNotificationToDb() async {
    NotificationModel notification = NotificationModel(
        title: task.value.title,
        body: task.value.note,
        date: task.value.date,
        task_id: task.value.id);
    var value = await addNotification(notification: notification);
    notification.id = value;
    return notification;
  }

  Future<int> addNotification({NotificationModel? notification}) async {
    return await DBHelper.insertNotification(notification);
  }
}
