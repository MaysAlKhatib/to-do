import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/Services/setting_services.dart';
import 'package:to_do_app/controllers/tasks_controller.dart';
import 'package:to_do_app/db/db_helper.dart';
import 'package:to_do_app/models/notification.dart';
import 'package:to_do_app/models/task.dart';

class AddTaskController extends GetxController {
  var task = Task().obs;
  late TextEditingController titleController;
  late TextEditingController noteController;
  var selectDate = DateTime.now().obs;
  var selectDateString = ''.obs;
  var endTime = "Select time".obs;
  var startTime = "Select time".obs;
  var selectedRemind = 5.obs;
  List<int> remindList = [5, 10, 15, 20];
  var selectedRepeat = 'None'.obs;
  List<String> repeatList = ['None', 'Daily', 'Weekly', 'Monthly'];
  var selectedColor = 0.obs;
  final formKey = GlobalKey<FormState>();

  SettingServices settingServices = Get.find();
  final TasksController _tasksController = Get.find();

  @override
  void onInit() {
    titleController = TextEditingController();
    noteController = TextEditingController();
    super.onInit();
  }

  Future<int> addTask() async {
    return await DBHelper.insertTask(task.value);
  }

  Future<int> addNotification({NotificationModel? notification}) async {
    return await DBHelper.insertNotification(notification);
  }

  @override
  void onClose() {
    titleController.dispose();
    noteController.dispose();
    super.onClose();
  }

  scheduleTask() async {
    NotificationModel notification;

    DateTime start = DateFormat.jm().parse(task.value.startTime.toString());
    var startTime = DateFormat("HH:mm").format(start);

    DateTime end = DateFormat.jm().parse(task.value.endTime.toString());
    var endTime = DateFormat("HH:mm").format(end);

    DateTime remind = DateFormat.jm()
        .parse(task.value.startTime.toString())
        .subtract(Duration(minutes: task.value.remind!));
    var remindTime = DateFormat("HH:mm").format(remind);

    notification = await _addTaskNotificationToDb();
    settingServices.notifyHelper.scheduledNotification(
        int.parse(startTime.toString().split(":")[0]),
        int.parse(startTime.toString().split(":")[1]),
        notification.id!.toInt(),
        task.value,
        'startTime');

    notification = await _addTaskNotificationToDb();
    settingServices.notifyHelper.scheduledNotification(
        int.parse(remindTime.toString().split(":")[0]),
        int.parse(remindTime.toString().split(":")[1]),
        notification.id!.toInt(),
        task.value,
        'reminder');

    notification = await _addTaskNotificationToDb();
    task.value.endTaskNotificationId = notification.id;
    await DBHelper.updateEndTaskNotificationId(
        task.value.id!, task.value.endTaskNotificationId!);
    settingServices.notifyHelper.scheduledNotification(
        int.parse(endTime.toString().split(":")[0]),
        int.parse(endTime.toString().split(":")[1]),
        notification.id!.toInt(),
        task.value,
        'endTime');
  }

  addTaskToDb() async {
    task = Task(
            title: titleController.text.toString(),
            note: noteController.text.toString(),
            date: DateFormat.yMd().format(selectDate.value),
            weekDay: selectDate.value.weekday,
            day: selectDate.value.day,
            startTime: startTime.value,
            endTime: endTime.value,
            remind: selectedRemind.value,
            repeat: selectedRepeat.value,
            color: selectedColor.value,
            isCompleted: 0)
        .obs;

    var value = await addTask();
    task.value.id = value;
    return task;
  }

  _addTaskNotificationToDb() async {
    NotificationModel notification = NotificationModel(
        title: task.value.title,
        body: task.value.note,
        date: task.value.date,
        task_id: task.value.id);
    var value = await addNotification(notification: notification);
    notification.id = value;
    return notification;
  }

  validateForm() async {
    if (formKey.currentState!.validate()) {
      await addTaskToDb();
      await scheduleTask();
      _tasksController.getTasks();
      Get.back();
    }
    }



  validateField(value,field) {
  if (value == null || value.isEmpty || value=='Select time') {
  return '$field is required';
  }
 return null;
  }

  NoValidation(value,String title) {
  return null;
  }

}
