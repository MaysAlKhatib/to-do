import 'package:get/get.dart';
import 'package:to_do_app/controllers/add_task_controller.dart';


class AddTaskBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() =>AddTaskController()
    ,fenix: true);
  }
}
