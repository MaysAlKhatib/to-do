import 'package:get/get.dart';
import 'package:to_do_app/controllers/add_task_controller.dart';
import 'package:to_do_app/controllers/notification_page_controller.dart';
import 'package:to_do_app/view/notification_page.dart';


class NotificationPageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() =>NotificationPageController()
        ,fenix: true);
  }
}
