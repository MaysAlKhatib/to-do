import 'package:get/get.dart';
import 'package:to_do_app/controllers/tasks_controller.dart';


class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(
        TasksController()
    );
  }
}
