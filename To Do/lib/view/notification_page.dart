import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_app/controllers/tasks_controller.dart';
import 'package:to_do_app/controllers/notification_page_controller.dart';
import 'package:to_do_app/utils/themes.dart';
import 'package:duration_picker/duration_picker.dart';

class NotificationPage extends StatelessWidget {
  final NotificationPageController _notificationPageController=Get.find();
  final TasksController _taskController = Get.put(TasksController());
  static String routeName='/notificationPage';

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (!_notificationPageController.isControllerReady.value) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [CircularProgressIndicator(color: primaryClr,)],
        );
      } else
        return Scaffold(
      appBar: AppBar(
        backgroundColor: Get.isDarkMode ? Colors.grey[700] : Colors.white,
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: Icon(Icons.arrow_back_ios),
          color: Get.isDarkMode ? Colors.white : Colors.grey,
        ),
        title: Text(
          _notificationPageController.task.value.title!,
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          SizedBox(height: 12),
          Container(
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(30),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: _getBGClr(_notificationPageController.task.value.color!),
              ),child: Column( children: [
                   Text(
                       _notificationPageController.task.value.title!,
                    style: GoogleFonts.lato(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Get.isDarkMode ? Colors.black : Colors.white,
                          fontSize: 18),

                  )),
                  SizedBox(height: 12),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.access_time_rounded,
                        color: Colors.grey[200],
                        size: 18,
                      ),
                      SizedBox(width: 4),
                      Text(
                        "${_notificationPageController.task.value.date!}",
                        style: GoogleFonts.lato(
                          textStyle:
                              TextStyle(fontSize: 13, color: Colors.grey[100]),
                        ),
                      ),
                      SizedBox(width: 14),
                      Text(
                        " ${_notificationPageController.task.value.startTime} - ${_notificationPageController.task.value.endTime}  ${_notificationPageController.task.value.repeat=='None'?' ':'(${_notificationPageController.task.value.repeat})'}",
                        style: GoogleFonts.lato(
                          textStyle:
                              TextStyle(fontSize: 13, color: Colors.grey[100]),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          _notificationPageController.task.value.note!,
                          style: GoogleFonts.lato(
                            textStyle: TextStyle(
                                color:
                                    Get.isDarkMode ? Colors.black : Colors.white,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ],
                  ),
                ]),
              ),
          Column(children: [
            SizedBox(height: 12),
                    if (_notificationPageController.task.value.isCompleted== 1)
                      Container()
            else
              _bigButton(
                  label: 'Task Completed',
                  onTap: () {
                    _taskController.markTaskCompleted(_notificationPageController.task.value.id!,
                        _notificationPageController.task.value.endTaskNotificationId!);
                    Get.back();
                  },
                  clr: primaryClr,
                  context: context),
            SizedBox(
              height: 20,
            ),
            if (_notificationPageController.task.value.isCompleted== 1)
              Container()
            else
            _bigButton(
                label: 'Remind me later',
                onTap:  () async {
                  var pickedDuration= (await showDurationPicker(
                    context: context,
                    initialTime: _notificationPageController.duration.value,
                  ))!;
                  if(pickedDuration!=null){
                    _notificationPageController.duration.value=pickedDuration;
                  }
                _notificationPageController.remindMe(true);
                  },
                clr: Colors.red[300]!,
                context: context),
            SizedBox(
              height: 20,
            ),
      Obx(() {
        print('confirm');
       if( _notificationPageController.remindMe.value)
         return _bigButton(
            label: 'Confirm',
            onTap: () {
              _notificationPageController.remindMeLater();
              Get.back();
            },
            isClose: true,
            clr: Colors.red[300]!,
            context: context);
       else
         return Container();

      }),SizedBox(
              height: 10,
            )          ])
        ]),
      ));});
  }
  _bigButton(
      {required String label,
        required Function()? onTap,
        required Color clr,
        bool isClose = false,
        required BuildContext context}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        height: 55,
        width: MediaQuery.of(context).size.width * 0.9,
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: isClose == true
                ? Get.isDarkMode
                ? Colors.grey[600]!
                : Colors.grey[300]!
                : clr,
          ),
          borderRadius: BorderRadius.circular(20),
          color: isClose == true ? Colors.transparent : clr,
        ),
        child: Center(
          child: Text(
            label,
            style:
            isClose ? titleStyle : titleStyle.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  _getBGClr(int no) {
    switch (no) {
      case 0:
        return bluishClr;
      case 1:
        return pinkClr;
      case 2:
        return yellowClr;
      default:
        return bluishClr;
    }
  }




}
