import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/Services/themes_services.dart';
import 'package:to_do_app/controllers/tasks_controller.dart';
import 'package:to_do_app/models/task.dart';
import 'package:to_do_app/view/add_task_page.dart';
import 'package:to_do_app/utils/themes.dart';
import 'package:to_do_app/view/widgets/button.dart';
import 'package:to_do_app/view/widgets/task_tile.dart';

class HomePage extends StatelessWidget {
  static String routeName='/';
  final TasksController _tasksController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.backgroundColor,
      appBar: _appBar(context),
      body: Column(
        children: [
          _addTaskBar(context),
          _addDatePicker(),
          SizedBox(
            height: 10,
          ),
          _showTasks(context)
        ],
      ),
    );
  }

  _addDatePicker() {
    return Container(
        margin: EdgeInsets.only(left: 20, top: 20),
        child: DatePicker(
          DateTime.now(),
          height: 100,
          width: 80,

          initialSelectedDate: DateTime.now(),
          selectionColor: primaryClr,
          selectedTextColor: Colors.white,
          //day number text style
          dateTextStyle: GoogleFonts.lato(
              textStyle: TextStyle(
            fontSize: 20,
            color: Colors.grey,
            fontWeight: FontWeight.w600,
          )),
          //day of week
          dayTextStyle: GoogleFonts.lato(
              textStyle: TextStyle(
            fontSize: 16,
            color: Colors.grey,
            fontWeight: FontWeight.w600,
          )),
          monthTextStyle: GoogleFonts.lato(
              textStyle: TextStyle(
            fontSize: 14,
            color: Colors.grey,
            fontWeight: FontWeight.w600,
          )),
          onDateChange: (date) {
              _tasksController.selectedDate.value = date;
              _tasksController.getTasks();
          },
        ));
  }

  _addTaskBar(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 20, left: 20, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                DateFormat.yMMMMd().format(DateTime.now()),
                textScaleFactor : MediaQuery.of(context).textScaleFactor,
                style: subHeadingStyle,
              ),
              Text(
                'Today',
                style: HeadingStyle,
              )
            ],
          ),
          MyButton(
              label: '+ Add Task',
              onTap: () async {
                await Get.toNamed(AddTaskPage.routeName);
              })
        ],
      ),
    );
  }

  _appBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: context.theme.backgroundColor,
      leading: GestureDetector(
        onTap: () {
          ThemeServices().switchTheme();
          _tasksController.changeThemeNotification();
          },
        child: Icon(
          Get.isDarkMode ? Icons.wb_sunny_outlined : Icons.nightlight_round,
          size: 20,
          color: Get.isDarkMode ? Colors.white : Colors.black,
        ),
      ),
    );
  }

  _showTasks(BuildContext context) {
    return Expanded(child: Obx(() {
      print('build show task');
      return _tasksController.taskList.length == 0
          ? _noTasks()
          : ListView.builder(
              itemCount: _tasksController.taskList.length,
              itemBuilder: (_, index) {
                Task task = _tasksController.taskList[index];
                return AnimationConfiguration.staggeredList(
                    position: index,
                    child: SlideAnimation(
                        child: FadeInAnimation(
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _showBottomSheet(context, task);
                            },
                            child: TaskTile(task),
                          )
                        ],
                      ),
                    )));
              });
    }));
  }

  _showBottomSheet(BuildContext context, Task task) {
    Get.bottomSheet(Container(
      padding: EdgeInsets.only(top: 4),
      height: task.isCompleted == 1
          ? MediaQuery.of(context).size.height * 0.24
          : MediaQuery.of(context).size.height * 0.32,
      color: Get.isDarkMode ? darkGreyClr : Colors.white,
      child: Column(
        children: [
          Container(
            height: 6,
            width: 120,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Get.isDarkMode ? Colors.grey[600] : Colors.grey[300]),
          ),
          Spacer(),
          if (task.isCompleted == 1)
            Container()
          else
            _bigButton(
                label: 'Task Completed',
                onTap: () {
                  _tasksController.markTaskCompleted(task.id!,task.endTaskNotificationId!);
                  Get.back();
                },
                clr: primaryClr,
                context: context),
          SizedBox(
            height: 20,
          ),
          _bigButton(
              label: 'Delete Task',
              onTap: () {
                _tasksController.deleteTask(task);
                Get.back();
              },
              clr: Colors.red[300]!,
              context: context),
          SizedBox(
            height: 20,
          ),
          _bigButton(
              label: 'Close',
              onTap: () {
                Get.back();
              },
              isClose: true,
              clr: Colors.red[300]!,
              context: context),
          SizedBox(
            height: 10,
          )
        ],
      ),
    ));
  }

  _bigButton(
      {required String label,
      required Function()? onTap,
      required Color clr,
      bool isClose = false,
      required BuildContext context}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        height: 55,
        width: MediaQuery.of(context).size.width * 0.9,
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: isClose == true
                ? Get.isDarkMode
                    ? Colors.grey[600]!
                    : Colors.grey[300]!
                : clr,
          ),
          borderRadius: BorderRadius.circular(20),
          color: isClose == true ? Colors.transparent : clr,
        ),
        child: Center(
          child: Text(
            label,
            style:
                isClose ? titleStyle : titleStyle.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  _noTasks() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'images/task.png',
            height: 150,
            color: Get.isDarkMode ? Colors.grey[600] : Colors.grey[350],
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Nothing to do',
            style: GoogleFonts.lato(
                textStyle: TextStyle(
              color: Get.isDarkMode ? Colors.grey[600] : Colors.grey[400],
              fontSize: 20,
            )),
          ),
          SizedBox(
            height: 80,
          ),
        ],
      ),
    );
  }
}
