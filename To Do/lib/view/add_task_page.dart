import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/controllers/add_task_controller.dart';
import 'package:to_do_app/utils/themes.dart';
import 'package:to_do_app/view/widgets/button.dart';
import 'package:to_do_app/view/widgets/input_field.dart';

class AddTaskPage extends StatelessWidget {
  static String routeName = '/addTaskPage';
  final AddTaskController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.backgroundColor,
      appBar: _appBar(context),
      body: Container(
        padding: const EdgeInsets.only(right: 20, left: 20),
        child: Form(
          key: _controller.formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Add Task',
                  style: HeadingStyle,
                ),
                MyInputField(
                  validator: _controller.validateField,
                  title: 'Title',
                  hint: 'Enter your title',
                  controller: _controller.titleController,
                ),
                MyInputField(
                  validator: _controller.NoValidation,
                  title: 'Note',
                  hint: 'Enter your note',
                  controller: _controller.noteController,
                ),
                Obx(() => MyInputField(
                      title: 'Date',
                      validator: _controller.validateField,
                      hint:
                          DateFormat.yMd().format(_controller.selectDate.value),
                      widget: IconButton(
                        icon: Icon(
                          Icons.calendar_today_outlined,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          _getDateFromUser(context);
                        },
                      ),
                    )),
                Row(
                  children: [
                    Obx(() => Expanded(
                        child: MyInputField(
                            validator: _controller.validateField,
                            title: 'Start Time',
                            widget: IconButton(
                              icon: Icon(
                                Icons.access_time_rounded,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                _getTimeFromUser(
                                    isStartTime: true, context: context);
                              },
                            ),
                            hint: _controller.startTime.value))),
                    SizedBox(width: 10),
                    Obx(() => Expanded(
                        child: MyInputField(
                            validator: _controller.validateField,
                            title: 'End Time',
                            widget: IconButton(
                              icon: Icon(
                                Icons.access_time_rounded,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                _getTimeFromUser(
                                    isStartTime: false, context: context);
                              },
                            ),
                            hint: _controller.endTime.value)))
                  ],
                ),
                Obx(() => MyInputField(
                      validator: _controller.NoValidation,
                      title: 'Remind',
                      hint: '${_controller.selectedRemind.value} minutes early',
                      widget: DropdownButton(
                        items: _controller.remindList
                            .map<DropdownMenuItem<String>>((int value) {
                          return DropdownMenuItem<String>(
                              value: value.toString(),
                              child: Text(value.toString()));
                        }).toList(),
                        icon:
                            Icon(Icons.keyboard_arrow_down, color: Colors.grey),
                        iconSize: 34,
                        underline: Container(
                          height: 0,
                        ),
                        elevation: 4,
                        style: subTitleStyle,
                        onChanged: (String? newValue) {
                          _controller.selectedRemind.value =
                              int.parse(newValue!);
                        },
                      ),
                    )),
                Obx(() => MyInputField(
                      validator: _controller.NoValidation,
                      title: 'Repeat',
                      hint: '${_controller.selectedRepeat.value}',
                      widget: DropdownButton(
                        items: _controller.repeatList
                            .map<DropdownMenuItem<String>>((String? value) {
                          return DropdownMenuItem<String>(
                              value: value!,
                              child: Text(
                                value,
                                style: TextStyle(color: Colors.grey),
                              ));
                        }).toList(),
                        icon:
                            Icon(Icons.keyboard_arrow_down, color: Colors.grey),
                        iconSize: 34,
                        underline: Container(
                          height: 0,
                        ),
                        elevation: 4,
                        style: subTitleStyle,
                        onChanged: (String? newValue) {
                          _controller.selectedRepeat.value = newValue!;
                        },
                      ),
                    )),
                SizedBox(
                  height: 18,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _colorPallete(),
                    MyButton(
                      label: 'Create Task',
                      onTap: () => _controller.validateForm(),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _colorPallete() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Color',
          style: titleStyle,
        ),

        SizedBox(
          height: 8,
        ),

        Obx(
          () => Wrap(
            children: List<Widget>.generate(3, (int index) {
              return GestureDetector(
                onTap: () {
                  _controller.selectedColor.value = index;
                },
                child: Padding(
                  padding: EdgeInsets.only(right: 8.0),
                  child: CircleAvatar(
                    child: _controller.selectedColor.value == index
                        ? Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 16,
                          )
                        : Container(),
                    radius: 14,
                    backgroundColor: index == 0
                        ? primaryClr
                        : index == 1
                            ? pinkClr
                            : yellowClr,
                  ),
                ),
              );
            }),
          ),
        )
      ],
    );
  }

  _appBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: context.theme.backgroundColor,
      leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Icon(
          Icons.arrow_back_ios,
          size: 20,
          color: Get.isDarkMode ? Colors.white : Colors.black,
        ),
      ),
    );
  }

  _getDateFromUser(BuildContext context) async {
    DateTime? _pickerDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2121),
        builder: (context, child) {
          return Theme(
            data: Theme.of(context).copyWith(
              colorScheme: Get.isDarkMode
                  ? ColorScheme.dark(primary: primaryClr, surface: primaryClr)
                  : ColorScheme.light(primary: primaryClr),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        });

    if (_pickerDate != null) {
      _controller.selectDate.value = _pickerDate;
    } else {
      print("it's null or something is wrong");
    }
  }

  _getTimeFromUser(
      {required bool isStartTime, required BuildContext context}) async {
    var pickedTime = await _showTimePicker(context);
    if (pickedTime != null) {
      String _hour = pickedTime.hour.toString();
      String _minute = pickedTime.minute.toString();
      String _time = _hour + ' : ' + _minute;
      _time = formatDate(
          DateTime(2019, 08, 1, pickedTime.hour, pickedTime.minute),
          [hh, ':', nn, " ", am]).toString();
      if (isStartTime == true) {
        _controller.startTime.value = _time;
      } else if (isStartTime == false) {
        _controller.endTime.value = _time;
      }
    }
  }

  _showTimePicker(BuildContext context) async {
    return showTimePicker(
        initialEntryMode: TimePickerEntryMode.dial,
        context: context,
        initialTime: TimeOfDay.now(),
        builder: (context, child) {
          return Theme(
            data: Theme.of(context).copyWith(
              colorScheme: Get.isDarkMode
                  ? ColorScheme.dark(
                      primary: primaryClr, surface: darkHeaderClr)
                  : ColorScheme.light(primary: primaryClr),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child!,
          );
        });
  }
}
