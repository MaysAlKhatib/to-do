import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:to_do_app/utils/binding/add_task_binding.dart';
import 'package:to_do_app/utils/binding/home_binding.dart';
import 'package:to_do_app/utils/binding/notification_page_binding.dart';
import 'package:to_do_app/view/add_task_page.dart';
import 'package:to_do_app/view/home_page.dart';
import 'package:to_do_app/utils/themes.dart';
import 'package:to_do_app/view/notification_page.dart';
import 'Services/setting_services.dart';
import 'Services/themes_services.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialServices();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    runApp(new MyApp());
  });
}

Future initialServices() async {
  await Get.putAsync(() => SettingServices().init());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'To Do',
        theme: Themes.light,
        darkTheme: Themes.dark,
        themeMode: ThemeServices().theme,
      initialRoute: HomePage.routeName,
      initialBinding: HomeBinding()
      ,getPages: [
      GetPage(name: HomePage.routeName, page: () => HomePage()),
      GetPage(name: AddTaskPage.routeName, page: () => AddTaskPage(),binding:AddTaskBinding()),
      GetPage(name: NotificationPage.routeName, page: () => NotificationPage(),binding:NotificationPageBinding()),
    ],);

  }
}
