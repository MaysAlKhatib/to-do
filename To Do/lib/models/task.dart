import 'dart:convert';

class Task {
  int? id;
  int? endTaskNotificationId;
  String? title;
  String? note;
  String? date;
  int? weekDay;
  int? day;
  String? startTime;
  String? endTime;
  String? repeat;
  int? isCompleted;
  int? color;
  int? remind;

  Task.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.endTaskNotificationId = json['finishNotificationId'];
    this.title = json['title'].toString();
    this.note = json['note'].toString();
    this.date = json['date'];
    this.weekDay = json['weekDay'];
    this.day = json['day'];
    this.startTime = json['startTime'];
    this.endTime = json['endTime'];
    this.isCompleted = json['isCompleted'];
    this.color = json['color'];
    this.remind = json['remind'];
    this.repeat = json['repeat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['finishNotificationId'] = this.endTaskNotificationId;
    data['title'] = this.title.toString();
    data['note'] = this.note.toString();
    data['date'] = this.date;
    data['weekDay'] = this.weekDay;
    data['day'] = this.day;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['isCompleted'] = this.isCompleted;
    data['color'] = this.color;
    data['remind'] = this.remind;
    data['repeat'] = this.repeat;
    return data;
  }

  factory Task.fromJsonString(String str) => Task.fromJson(jsonDecode(str));

  String toJsonString() => jsonEncode(toJson());

  Task(
      {this.id,
      this.title,
      this.note,
      this.date,
      this.startTime,
      this.endTime,
      this.repeat,
      this.isCompleted,
      this.color,
      this.remind,
      this.weekDay,
      this.day});
}
