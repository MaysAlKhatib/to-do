import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:to_do_app/models/notification.dart';
import 'package:to_do_app/models/task.dart';

class DBHelper {
  static Database? _db;
  static final int _version = 1;
  static final String _taskTable = 'tasks';
  static final String _notificationsTable = 'notifications';

  static Future<void> initDB() async {
    if (_db != null) {
      return;
    }
    try {
      String _path = await getDatabasesPath() + 'toDo.db';
      _db =
          await openDatabase(_path, version: _version, onCreate: (db, version) {
        print('create a new one ');

        db.execute("CREATE TABLE $_taskTable("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "title STRING, note TEXT, date STRING,"
            "weekDay INTEGER, day INTEGER,"
            "startTime STRING, endTime STRING,"
            "remind INTEGER, repeat STRING,"
            "color INTEGER,"
            "finishNotificationId INTEGER,"
            "isCompleted INTEGER)");

        db.execute("CREATE TABLE $_notificationsTable("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "title STRING, body TEXT, date STRING,"
            "task_id INTEGER,"
            "FOREIGN KEY(task_id) REFERENCES $_taskTable(id))");
      });
    } catch (e) {
      print(e);
    }
  }

  static Future<int> insertTask(Task? task) async {
    return await _db?.insert(_taskTable, task!.toJson()) ?? 1;
  }

  static Future<int> insertNotification(NotificationModel? notification) async {
    return await _db?.insert(_notificationsTable, notification!.toJson()) ?? 1;
  }

  static Future<List<Map<String, dynamic>>> queryAllTasks() async {
    return await _db!.query(_taskTable);
  }

  static Future<List<Map<String, dynamic>>> queryTasks(
      DateTime selectedDate) async {
    print('queryTasks function called');
    var result = await _db!.rawQuery(
        'SELECT * FROM $_taskTable WHERE date=? OR repeat=? OR (weekDay=? AND repeat=?) OR (day=? AND repeat=?)',
        [
          DateFormat.yMd().format(selectedDate),
          'Daily',
          selectedDate.weekday,
          'Weekly',
          selectedDate.day,
          'Monthly'
        ]);
    print('print queryTasks');
    result.forEach((row) async {
      print(row);
    });
    return result;
  }

  static Future<List<Map<String, dynamic>>> queryAllNotifications() async {
    return await _db!.query(_notificationsTable);
  }

  static deleteNotificationDB(int id) async {
    await _db!.delete(_notificationsTable, where: 'id=?', whereArgs: [id]);
  }

  static delete(Task task) async {
    await _db!.delete(_taskTable, where: 'id=?', whereArgs: [task.id]);
    return await _db!
        .delete(_notificationsTable, where: 'task_id=?', whereArgs: [task.id]);
  }

  static update(int id) async {
    await _db!.rawUpdate('''
    UPDATE tasks
    SET isCompleted = ?
    WHERE id =?
    ''', [1, id]);
  }

  static updateEndTaskNotificationId(int taskId, int finishId) async {
    await _db!.rawUpdate('''
    UPDATE tasks
    SET finishNotificationId = ?
    WHERE id =?
    ''', [finishId, taskId]);
  }

  static queryTaskNotifications(Task task) async {
    List<Map> result = await _db!.rawQuery(
        'SELECT id FROM $_notificationsTable WHERE task_id=?', [task.id]);
    return result;
  }

  static getTaskById(int id) async {
    final maps = await _db!.query(_taskTable, where: 'id = ?', whereArgs: [id]);
    var task = Task.fromJson(maps[0]);
    return task;
  }
}
