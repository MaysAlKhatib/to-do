# To Do
## **Organize your tasks :**
- #### with a daily, weekly, monthly or one-time recurrence.
 
- ##### With a reminder notification before the start of the task, when it starts and when it ends _**(Scheduled notifications)**_.
- #### you can mark a task as completed or reminding you of it later with a notification.

- #### In addition to the light and night mode.

# Using
- Getx
- Sqflite
- Flutter local notifications

# ScreenShots
### home page
<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/3.jpg" width="250">


<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/4.jpg" width="250">

### add task page
<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/1.jpg" width="250">


<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/2.jpg" width="250">

### manage task 
<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/5.jpg" width="250">

### notification page
<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/7.png" width="250">

### remind me later 
<img src="https://gitlab.com/MaysAlKhatib/to-do/-/raw/main/To%20Do/images/ScreenShots/6.jpg" width="250">
